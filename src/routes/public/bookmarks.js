import { Router } from 'express';
import models from '../../models';
import constants from '../../constants';
const axios = require('axios');

const router = Router();

/**
 * Получить get параметры
 */
const getParams = (req) => {
  let data = {
    limit: req.query.limit ? req.query.limit : 50,
    offset: req.query.offset ? req.query.offset : 0,
    filter: req.query.filter ? req.query.filter : '',
    filter_value: req.query.filter_value ? req.query.filter_value : false,
    sort_by: req.query.sort_by ? req.query.sort_by : 'createdAt',
    sort_dir: req.query.sort_dir ? req.query.sort_dir : 'asc'
  }
  return data;
}

/**
 * Получить массив со стороннего API ресурса
 */
const getWhoisData = async (link) => {
  try {
    let res = await axios.get(`https://htmlweb.ru/analiz/api.php?whois&url=${link}&json`);
    return res.data;
  } catch(error) {
    return {
      code: error.code,
      message: error.message
    };
  }
 
}
/**
 * @api {get} [dev-]backend.wazzup24.com/api/v1/bookmarks get all bookmarks
 * @apiName get bookmarks
 * @apiDescription Вывод списка закладок
 * @apiVersion 1.0.0
 * 
 * @apiSuccessExample SUCCESS:
 *   HTTP/1.1 200 OK
 *   {
 *     "length": 46,
 *      data": [
 *        {
 *          "guid": "da611c3b-0888-495a-91f6-cd8292cebd3e",
 *          "link": "https://google.com",
 *          "createdAt": "2020-08-05T15:56:13.423Z",
 *          "updatedAt": "2020-08-07T12:21:10.008Z",
 *          "description": "google description",
 *          "favorites": true
 *        },
 *        {
 *          "guid": "bb4964ff-ba23-4e15-9a83-bf58eb86487e",
 *          "link": "https://amazon.com",
 *          "createdAt": "2020-08-06T03:56:20.423Z",
 *          "updatedAt": "2020-08-06T03:56:20.008Z",
 *          "description": " amazon description",
 *          "favorites": true
 *        },
 *      ]
 *   }
 *      
 * */
router.get("/", async (req, res) => {

  try {
    let params = getParams(req);
    let filter_condition;
    
    if (params.filter == 'favorites') {
      let filter_value = params.filter_value == 'true' ? true : false
      filter_condition = {
        $and: {favorites: Boolean(filter_value)}
      };
    }

    await models.bookmark.findAndCountAll({
      attributes: ['guid', 'link', 'createdAt', 'description', 'favorites'],
      limit: params.limit, 
      offset: params.offset, 
      order: [[params.sort_by, params.sort_dir]],
      where: filter_condition
    })
    .then(results => {
      res.json({
        length: results.count,
        data: results.rows,
      })
    });
  } catch(error) {
    res.status(400).json({
      status: 'error',
      error: 'req body cannot be empty',
    });
  }
});

/**
 * @api {post} [dev-]backend.wazzup24.com/api/v1/bookmarks create bookmark
 * @apiName create bookmark
 * @apiDescription Добавление закладки
 * @apiVersion 1.0.0
 * 
 * @apiSuccessExample SUCCESS:
 *  HTTP/1.1 200 OK
 *  {
 *      data: {
 *        "guid": "e47dd106-ecc8-42b3-9d2d-6278c72aef76",
 *        "createdAt": "2020-08-08T03:46:28.825Z"
 *      }
 *  }
 * 
 * @apiErrorExample ALL EXAMPLES:
 *   HTTP/1.1 400 Bad Request
 *   {
  *    "errors": {
          "code": "BOOKMARKS_INVALID_LINK",
          "description": "Invalid link"
        },

        "errors": {
          "code": "BOOKMARKS_BLOCKED_DOMAIN",
          "description": "yahoo.com, socket.io banned"
        }
 *   }
 *      
 * */
router.post("/", async (req, res) => {
  try {
    let link = req.body.link;
    let description = req.body.description;      
    let favorites = req.body.favorites;
    let data = {
      link: link,
      description: description,
      favorites: favorites,
      og_title: req.body.openGraph.title,
      og_image: req.body.openGraph.image,
      og_description: req.body.openGraph.description,
    }
    let bookmark = await models.bookmark.create(data).catch(err => {

      if(err.errors) {
        let error = err.errors[0];
        let code;
        error['validatorKey'] == 'not' ? code = constants.BOOKMARKS_BLOCKED_DOMAIN : '';
        error['validatorKey'] == 'is' ? code = constants.BOOKMARKS_INVALID_LINK : '';
  
        res.status(400).json({ errors: {
            code: code,
            description: error['message'],
          } 
        });
      }
    });
    res.json(bookmark)
    if(bookmark) {
      res.status(201).json({ data: {
        guid: bookmark.guid,
        createdAt: bookmark.createdAt,
      }});
    }
  } catch(error) {
    res.status(400).json({ errors: {
      code: constants.BOOKMARKS_BAD_REQUEST,
      description: constants.BOOKMARKS_INCORRECT_DATA_TEXT,
    }});
  }
});

/**
 * @api {patch} [dev-]backend.wazzup24.com/api/v1/bookmarks/:guid update bookmark by guid
 * @apiName update bookmark
 * @apiDescription Изменение закладки по guid
 * @apiVersion 1.0.0
 * @apiParam {String} guid идентификатор записи
 * 
 * @apiSuccessExample SUCCESS:
 *  HTTP/1.1 200 OK
 *  {
 *    "code": "BOOKMARKS_SUCCESS_UPDATE",
 *    "description": "Запись успешна изменена!"
 *  }
 * 
 * @apiErrorExample ALL EXAMPLES:
 *   HTTP/1.1 400 Bad Request
 *   {
 *      "errors": {
 *         "code": "BOOKMARKS_INVALID_LINK",
 *         "description": "Invalid link"
 *       },
 *       "errors": {
 *         "code": "BOOKMARKS_BLOCKED_DOMAIN",
 *         "description": "yahoo.com, socket.io banned"
 *       },
 *       "errors": {
 *         "code": "BOOKMARKS_BOOLEAN_ERROR",
 *         "description": "only BOLLEAN format"
 *       }
 *   }
 * @apiErrorExample ALL EXAMPLES:
 *    HTTP/1.1 404 NOT FOUND
 *    {
 *      "errors": {
 *        "code": "BOOKMARKS_NOT_FOUND",
 *        "description": "Нет записи с таким ID"
 *       },
 *    }
 *      
 * */
router.patch("/:guid", async (req, res) => {

  try {
    let id = req.params.guid;
    let bookmark = await models.bookmark.findOne({ where: {guid: id}});

    if(bookmark) {
      let link = req.body.link;
      let description = req.body.description;
      let favorites = req.body.favorites;
  
      let data = {link: link, description: description, favorites: favorites};
      let result = await models.bookmark.update(data, {where: {guid: id}});
  
      if(result) {
        res.status(200).json({ 
          code: constants.BOOKMARKS_SUCCESS_UPDATE,
          description: constants.BOOKMARKS_UPDATE_SUCCESS_TEXT,
        });
      }
    }
    else {
      res.status(404).json({ errors: {
        code: constants.BOOKMARKS_NOT_FOUND,
        description: constants.BOOKMARKS_NOT_FOUND_ID_TEXT,
      }});
    }
  } catch(error) {
    if(error.errors) {
      let err = error.errors[0];
      let code = '';
      err['validatorKey'] == 'not' ? code = constants.BOOKMARKS_BLOCKED_DOMAIN : '';
      err['validatorKey'] == 'is' ? code = constants.BOOKMARKS_INVALID_LINK : '';
      err['validatorKey'] == 'isIn' ? code = constants.BOOKMARKS_BOOLEAN_ERROR : '';
      res.status(400).json({ errors: {
        code: code,
        description: err['message'],
      }});
    }
  }
});

/**
 * @api {get} [dev-]backend.wazzup24.com/api/v1/bookmarks/:guid get bookmark by guid
 * @apiName get bookmark
 * @apiDescription Получение закладки по guid
 * @apiVersion 1.0.0
 * @apiParam {String} guid идентификатор записи
 * 
 * @apiSuccessExample SUCCESS:
 *  HTTP/1.1 200 OK
 *  {
 *    "code": "BOOKMARKS_SUCCESS_FIND",
 *    "description": "Запись успешна найдена!",
 *    "data": {
 *        "guid": "da611c3b-0888-495a-91f6-cd8292cebd3e",
 *         "link": "https://sockesssfe.russ",
 *         "createdAt": "2020-08-05T15:56:13.423Z",
 *         "updatedAt": "2020-08-07T12:21:10.008Z",
 *         "description": " mma MIX",
 *         "favorites": true
 *    }
 *  }
 * 
 * @apiErrorExample ALL EXAMPLES:
 *   HTTP/1.1 404 NOT FOUND
 *   {
 *      "errors": {
 *          "code": "BOOKMARKS_NOT_FOUND",
 *          "description": "Нет записи с таким ID"
 *       },
 *   }
 *      
 * */
router.get("/:guid", async (req, res) => {
   
  try {
    let id = req.params.guid;
    const bookmark = await models.bookmark.findOne({ where: { guid: id }});

    if(bookmark) {
      const whoisData = await getWhoisData(bookmark.link);
      res.status(200).json({ 
        code: constants.BOOKMARKS_SUCCESS_FIND,
        description: constants.BOOKMARKS_FOUND_RECORD_TEXT,
        guid: bookmark.guid,
        data: {
          'whoisData' : whoisData,
          'openGraph' : {title: bookmark.og_title, image: bookmark.og_image, description: bookmark.og_description}
        }
      });
    } else {
      res.status(404).json({ errors: {
        code: constants.BOOKMARKS_NOT_FOUND,
        description: constants.BOOKMARKS_NOT_FOUND_ID_TEXT,
      }}); 
    }
  } catch(error) {
      res.status(400).json({ errors: {
        code: constants.BOOKMARKS_BAD_REQUEST,
        description: error,
      }});  
    }
});

/**
 * @api {delete} [dev-]backend.wazzup24.com/api/v1/bookmarks/:guid delete bookmark by guid
 * @apiName delete bookmark
 * @apiDescription Удаление закладки по guid
 * @apiVersion 1.0.0
 * @apiParam {String} guid идентификатор записи
 * 
 * @apiSuccessExample SUCCESS:
 *  HTTP/1.1 200 OK
 *  {
 *      "code": "BOOKMARKS_SUCCESS_DELETE",
 *      "description": "Запись успешна удалена!"
 *  }
 * 
 * @apiErrorExample ALL EXAMPLES:
 *   HTTP/1.1 404 NOT FOUND
 *   {
 *      "errors": {
 *        "code": "BOOKMARKS_NOT_FOUND",
 *        "description": "Нет записи с таким ID"
 *      },
 *   }
 * @apiErrorExample ALL EXAMPLES:
 *  HTTP/1.1 400 Bad Request
 *  {
 *      "errors": {
 *         "code": "BOOKMARKS_NOT_FOUND",
 *         "description": "Нет записи с таким ID"
 *       },
 *  }
 *      
 * */
router.delete("/:guid", async (req, res) => {

  try {
    let id = req.params.guid;
    let bookmark = await models.bookmark.findOne({ where: {guid: id}});

    if (bookmark) {
      let result = await models.bookmark.destroy({where: {guid: id}});
      if(result) {
        res.status(200).json({ 
          code: constants.BOOKMARKS_SUCCESS_DELETE, 
          description: constants.BOOKMARKS_DELETE_SUCCESS_TEXT
        });
      }
    }
    res.status(404).json({ errors: {
      code: constants.BOOKMARKS_NOT_FOUND,
      description: constants.BOOKMARKS_NOT_FOUND_ID_TEXT,
    }});
  } catch(error) {
    res.status(400).json({ errors: {
      code: constants.BOOKMARKS_BAD_REQUEST,
      description: constants.BOOKMARKS_INCORRECT_DATA_TEXT,
    }});   
   }
});

export default router;
