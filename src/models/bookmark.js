'use strict';

module.exports = (sequelize, DataTypes) => {
  var bookmark = sequelize.define('bookmark', {
    guid: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4
      },
      link: {
        type: DataTypes.STRING(256),
        validate: {
          is: { 
            args: [/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)/],
            msg: "Invalid link"
          },
          not: { 
            args: [/^(https?|http):\/\/(www\.)?yahoo.com|socket.io*$/],
            msg: "yahoo.com, socket.io banned"
          },
        }
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
      },
      description: {
        type: DataTypes.TEXT,
      },
      favorites: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        validate : {
          isIn: { 
            args: [[true, false]],
            msg: "only BOLLEAN format"
          }
        }
      },
      og_title: {
        type: DataTypes.STRING(256),
      },
      og_image: {
        type: DataTypes.STRING(256),
      },
      og_description: {
        type: DataTypes.TEXT,
      },
  });
  return bookmark;
};
