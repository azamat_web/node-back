define({ "api": [
  {
    "type": "post",
    "url": "[dev-]backend.wazzup24.com/api/v1/bookmarks",
    "title": "create bookmark",
    "name": "create_bookmark",
    "description": "<p>Добавление закладки</p>",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "SUCCESS:",
          "content": "HTTP/1.1 200 OK\n{\n    data: {\n      \"guid\": \"e47dd106-ecc8-42b3-9d2d-6278c72aef76\",\n      \"createdAt\": \"2020-08-08T03:46:28.825Z\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ALL EXAMPLES:",
          "content": "HTTP/1.1 400 Bad Request\n{\n \"errors\": {\n        \"code\": \"BOOKMARKS_INVALID_LINK\",\n        \"description\": \"Invalid link\"\n      },\n\n      \"errors\": {\n        \"code\": \"BOOKMARKS_BLOCKED_DOMAIN\",\n        \"description\": \"yahoo.com, socket.io banned\"\n      }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes/public/bookmarks.js",
    "group": "/var/www/backend-node/src/routes/public/bookmarks.js",
    "groupTitle": "/var/www/backend-node/src/routes/public/bookmarks.js"
  },
  {
    "type": "delete",
    "url": "[dev-]backend.wazzup24.com/api/v1/bookmarks/:guid",
    "title": "delete bookmark by guid",
    "name": "delete_bookmark",
    "description": "<p>Удаление закладки по guid</p>",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guid",
            "description": "<p>идентификатор записи</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "SUCCESS:",
          "content": "HTTP/1.1 200 OK\n{\n    \"code\": \"BOOKMARKS_SUCCESS_DELETE\",\n    \"description\": \"Запись успешна удалена!\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ALL EXAMPLES:",
          "content": "HTTP/1.1 404 NOT FOUND\n{\n   \"errors\": {\n     \"code\": \"BOOKMARKS_NOT_FOUND\",\n     \"description\": \"Нет записи с таким ID\"\n   },\n}",
          "type": "json"
        },
        {
          "title": "ALL EXAMPLES:",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"errors\": {\n       \"code\": \"BOOKMARKS_NOT_FOUND\",\n       \"description\": \"Нет записи с таким ID\"\n     },\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes/public/bookmarks.js",
    "group": "/var/www/backend-node/src/routes/public/bookmarks.js",
    "groupTitle": "/var/www/backend-node/src/routes/public/bookmarks.js"
  },
  {
    "type": "get",
    "url": "[dev-]backend.wazzup24.com/api/v1/bookmarks/:guid",
    "title": "get bookmark by guid",
    "name": "get_bookmark",
    "description": "<p>Получение закладки по guid</p>",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guid",
            "description": "<p>идентификатор записи</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "SUCCESS:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": \"BOOKMARKS_SUCCESS_FIND\",\n  \"description\": \"Запись успешна найдена!\",\n  \"data\": {\n      \"guid\": \"da611c3b-0888-495a-91f6-cd8292cebd3e\",\n       \"link\": \"https://sockesssfe.russ\",\n       \"createdAt\": \"2020-08-05T15:56:13.423Z\",\n       \"updatedAt\": \"2020-08-07T12:21:10.008Z\",\n       \"description\": \" mma MIX\",\n       \"favorites\": true\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ALL EXAMPLES:",
          "content": "HTTP/1.1 404 NOT FOUND\n{\n   \"errors\": {\n       \"code\": \"BOOKMARKS_NOT_FOUND\",\n       \"description\": \"Нет записи с таким ID\"\n    },\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes/public/bookmarks.js",
    "group": "/var/www/backend-node/src/routes/public/bookmarks.js",
    "groupTitle": "/var/www/backend-node/src/routes/public/bookmarks.js"
  },
  {
    "type": "get",
    "url": "[dev-]backend.wazzup24.com/api/v1/bookmarks",
    "title": "get all bookmarks",
    "name": "get_bookmarks",
    "description": "<p>Вывод списка закладок</p>",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "SUCCESS:",
          "content": "HTTP/1.1 200 OK\n{\n  \"length\": 46,\n   data\": [\n     {\n       \"guid\": \"da611c3b-0888-495a-91f6-cd8292cebd3e\",\n       \"link\": \"https://google.com\",\n       \"createdAt\": \"2020-08-05T15:56:13.423Z\",\n       \"updatedAt\": \"2020-08-07T12:21:10.008Z\",\n       \"description\": \"google description\",\n       \"favorites\": true\n     },\n     {\n       \"guid\": \"bb4964ff-ba23-4e15-9a83-bf58eb86487e\",\n       \"link\": \"https://amazon.com\",\n       \"createdAt\": \"2020-08-06T03:56:20.423Z\",\n       \"updatedAt\": \"2020-08-06T03:56:20.008Z\",\n       \"description\": \" amazon description\",\n       \"favorites\": true\n     },\n   ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes/public/bookmarks.js",
    "group": "/var/www/backend-node/src/routes/public/bookmarks.js",
    "groupTitle": "/var/www/backend-node/src/routes/public/bookmarks.js"
  },
  {
    "type": "patch",
    "url": "[dev-]backend.wazzup24.com/api/v1/bookmarks/:guid",
    "title": "update bookmark by guid",
    "name": "update_bookmark",
    "description": "<p>Изменение закладки по guid</p>",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guid",
            "description": "<p>идентификатор записи</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "SUCCESS:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": \"BOOKMARKS_SUCCESS_UPDATE\",\n  \"description\": \"Запись успешна изменена!\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ALL EXAMPLES:",
          "content": "HTTP/1.1 400 Bad Request\n{\n   \"errors\": {\n      \"code\": \"BOOKMARKS_INVALID_LINK\",\n      \"description\": \"Invalid link\"\n    },\n    \"errors\": {\n      \"code\": \"BOOKMARKS_BLOCKED_DOMAIN\",\n      \"description\": \"yahoo.com, socket.io banned\"\n    },\n    \"errors\": {\n      \"code\": \"BOOKMARKS_BOOLEAN_ERROR\",\n      \"description\": \"only BOLLEAN format\"\n    }\n}",
          "type": "json"
        },
        {
          "title": "ALL EXAMPLES:",
          "content": "HTTP/1.1 404 NOT FOUND\n{\n  \"errors\": {\n    \"code\": \"BOOKMARKS_NOT_FOUND\",\n    \"description\": \"Нет записи с таким ID\"\n   },\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes/public/bookmarks.js",
    "group": "/var/www/backend-node/src/routes/public/bookmarks.js",
    "groupTitle": "/var/www/backend-node/src/routes/public/bookmarks.js"
  },
  {
    "type": "get",
    "url": "[dev-]backend.wazzup24.com/api",
    "title": "Get API information",
    "description": "<p>Получение информации об API</p>",
    "version": "1.0.0",
    "name": "version",
    "group": "API",
    "permission": [
      {
        "name": "all"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "SUCCESS:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": {\n    \"version\": \"1\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes/public/api.js",
    "groupTitle": "API"
  },
  {
    "type": "get",
    "url": "[dev-]backend.wazzup24.com/api/v1/channels",
    "title": "List of channels",
    "description": "<p>Получение списка каналов пользователя</p>",
    "version": "1.0.0",
    "name": "channels_list",
    "group": "Channels",
    "permission": [
      {
        "name": "all"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Значение accessToken, полученное в процессе аутентификации</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>Смещение начала выборки (с какого по счету, по умолчанию 0)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Ограничение выборки (как много элементов добавить в выборку, по умолчанию 10)</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "fields",
            "description": "<p>Массив имен полей объектов, которые добавляются в результирующий ответ. Если не указывать - все поля.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "SUCCESS:",
          "content": "HTTP/1.1 200 OK\n{ \n  \"data\": [\n    {\n      \"guid\": \"e40b62b5-6fea-4993-b894-ed7484458edc\",\n      \"accountId\": 12,\n      \"createdAt\": \"2018-05-14T19:20:54.841Z\",\n      \"name\": \"break\",\n      \"transport\": \"telegram\",\n      \"state\": \"active\",\n      \"temporary\": true\n    }, ...\n    {\n      \"guid\": \"d40b62b5-6fea-4993-b894-ed7484458edc\",\n      \"accountId\": 22,\n      \"createdAt\": \"2018-05-14T19:20:54.841Z\",\n      \"name\": \"test\",\n      \"transport\": \"whatsapp\",\n      \"state\": \"active\",\n      \"temporary\": true\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ALL EXAMPLES:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"errors\": {\n    \"limit\": [\n      \"Limit is not a number\",\n      \"Limit must be greater than or equal to 0\"\n    ],\n    \"offset\": [\n      \"Offset is not a number\",\n      \"Offset must be greater than or equal to 0\"\n    ],\n    \"fields\": [\n      \"Fields is not array\",\n      \"Fields is not array of string\",\n      \"Fields is not contains guid,accountId,createdAt,name,phone,transport,state,notEnoughMoney,tarif,temporary\"\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes/public/channels.js",
    "groupTitle": "Channels"
  },
  {
    "type": "get",
    "url": "[dev-]backend.wazzup24.com/api/v1/emailTokens/:token/handle",
    "title": "Handle email token",
    "description": "<p>Обработка перехода из письма</p>",
    "version": "1.0.0",
    "name": "handle-email-token",
    "group": "EmailTokens",
    "permission": [
      {
        "name": "all"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Email token из письма</p>"
          }
        ]
      }
    },
    "filename": "src/routes/public/emailTokens.js",
    "groupTitle": "EmailTokens"
  },
  {
    "type": "post",
    "url": "[dev-]backend.wazzup24.com/api/v1/promoCodes/random",
    "title": "Generate random promoCode",
    "description": "<p>Генерация случайного уникального промокода</p>",
    "version": "1.0.0",
    "name": "generate_random_promo_code",
    "group": "PromoCodes",
    "permission": [
      {
        "name": "all"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "SUCCESS:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": {\n    \"code\": \"4pvugb\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ALL EXAMPLES:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"errors\": {\n    \"backend\": [\n      \"Can't generate promoCode\",\n      \"No more attempts\"\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes/public/promoCodes.js",
    "groupTitle": "PromoCodes"
  },
  {
    "type": "get",
    "url": "[dev-]backend.wazzup24.com/api/v1/users/:guid/password/change",
    "title": "Change password",
    "description": "<p>Фактическое изменение пароля</p>",
    "version": "1.0.0",
    "name": "change-password",
    "group": "Users",
    "permission": [
      {
        "name": "all"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Значение accessToken, полученное в процессе аутентификации</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hashPassword",
            "description": "<p>Пароль пользователя, полученный с помощью функции хеширования</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "SUCCESS:",
          "content": "HTTP/1.1 200 OK\n{\n  \"expiresIn\": 3600,\n  \"accessToken\": '...',\n  \"refreshToken\": '...'\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ALL EXAMPLES:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"errors\": {\n    \"email\": [\n      \"Email can't be blank\",\n      \"Email is not a valid email\"\n    ],\n    \"hashPassword\": [\n      \"Hash password can't be blank\",\n      \"Hash password length must be 64\"\n    ],\n    \"backend\": [\n      \"User not found\",\n      \"Can not delete active tokens\"\n      \"Can not generate tokens\"\n      \"Can not delete active tokens\"\n      \"Email not equal\"\n      \"Email not confirmed\"\n      \"Can not delete active tokens\"\n      \"Can't change password\"\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes/public/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "[dev-]backend.wazzup24.com/api/v1/users/password/recovery",
    "title": "Password recovery",
    "description": "<p>Восстановление пароля</p>",
    "version": "1.0.0",
    "name": "password_recovery",
    "group": "Users",
    "permission": [
      {
        "name": "all"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email пользователя</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "SUCCESS:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ALL EXAMPLES:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"errors\": {\n    \"email\": [\n      \"Email can't be blank\",\n      \"Email is not a valid email\"\n    ],\n    \"backend\": [\n      \"User not found\",\n      \"User not found\",\n      \"User is blocked\",\n      \"Email not confirmed\",\n      \"Role 'owner' not found\",\n      \"Account email is not equal\",\n      \"Account is blocked\",\n      \"Template is not exist\",\n      \"Can't recovery password\",\n      \"Email not confirmed\"\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes/public/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "[dev-]backend.wazzup24.com/api/v1/users/signup",
    "title": "Signup user",
    "description": "<p>Регистрация пользователя</p>",
    "version": "1.0.0",
    "name": "signup",
    "group": "Users",
    "permission": [
      {
        "name": "all"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hashPassword",
            "description": "<p>Пароль пользователя, полученный с помощью функции хеширования</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>Двухсимвольный идентификатор языка</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "promo",
            "description": "<p>Промокод, указанный при регистрации [необязательное поле]</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ref",
            "description": "<p>Ссылка [необязательное поле]</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "SUCCESS:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ALL EXAMPLES:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"errors\": {\n    \"email\": [\n      \"Email can't be blank\",\n      \"Email is not a valid email\"\n    ],\n    \"hashPassword\": [\n      \"Hash password can't be blank\",\n      \"Hash password length must be 64\"\n    ],\n    \"promo\": [\"Promo is too long (maximum is 50 characters)\"]\n    \"lang\": [\n      \"Lang can't be blank\",\n      \"Lang length must be 2\"\n     ],\n    \"backend\": [\n      \"User is exist\"\n      \"Promo is not exist\"\n      \"Promo is old\"\n      \"Template is not exist\"\n      \"Can't register user\"\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes/public/users.js",
    "groupTitle": "Users"
  }
] });
